<?php
/**
Template Name: Contact Us
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>


<div class="contactus">
    <div class="sitecontainer">
        <div class="contactusbox">
            <div class="heading">
                <h2><?php echo CFS()->get('contact_us_title'); ?></h2>
                <p><?php echo CFS()->get('contact_us_tagline'); ?></p>
            </div>
            <div class="contactinfo">
                <div class="leftside">
                    <div class="item address">
                        <?php $address = CFS()->get('address'); ?>
                        <?php if($address != '') { ?>
                        <label><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contactus_location.png"> Address</label>
                        <p><?php echo CFS()->get('address'); ?></p>
                        <?php } ?>
                    </div>
                    <div class="item phonefax">
                        <?php $phone1 = CFS()->get('phone'); ?>
                        <?php if($phone1 != '') { ?>
                        <div class="halfitem">
                            <label><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact_usphone.png"> Phone</label>
                            <p><a href="tel:<?php echo CFS()->get('phone'); ?>"><?php echo CFS()->get('phone'); ?></a></p>
                        </div>
                        <?php } ?>
                        <?php $fax1 = CFS()->get('fax'); ?>
                        <?php if($fax1 != '') { ?>
                        <div class="halfitem">
                            <label><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact us_fax.png"> Fax</label>
                            <p><?php echo CFS()->get('fax'); ?></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="item enquiry">
                        <p>Advertising Enquiries</p>
                        <?php $phone2 = CFS()->get('advertising_enquiries_phone'); ?>
                        <?php if($phone2 != '') { ?>
                        <label><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact_usphone.png">
                            <a href="tel:<?php echo CFS()->get('advertising_enquiries_phone'); ?>"> 
                            <?php echo CFS()->get('advertising_enquiries_phone'); ?>
                            </a>
                        </label>
                        <?php } ?>

                        <?php $email2 = CFS()->get('advertising_enquiries_email'); ?>
                        <?php if($email2 != '') { ?>
                        <label>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact us_email.png"> 
                            <a href="mailto:<?php echo CFS()->get('advertising_enquiries_email'); ?>">
                            <?php echo CFS()->get('advertising_enquiries_email'); ?>
                            </a>
                        </label>
                        <?php } ?>

                    </div>
                    <div class="item enquiry">
                        <p>General Enquiries</p>

                        <?php $phone3 = CFS()->get('general_enquiries_phone'); ?>
                        <?php if($phone3 != '') { ?>
                        <label><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact_usphone.png"> 
                            <a href="tel:<?php echo CFS()->get('general_enquiries_phone'); ?>">
                            <?php echo CFS()->get('general_enquiries_phone'); ?>
                            </a>
                        </label>
                        <?php } ?>

                        <?php $email3 = CFS()->get('general_enquiries_email'); ?>
                        <?php if($email3 != '') { ?>
                        <label style="padding-left: 20px;"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact us_email.png"> 
                            <a href="mailto:<?php echo CFS()->get('general_enquiries_email'); ?>">
                            <?php echo CFS()->get('general_enquiries_email'); ?>
                            </a>
                        </label>
                        <?php } ?>


                    </div>
                    <div class="item">
                        <p>Editorial and iSPY submissions</p>
                        <?php $email4 = CFS()->get('editorial_and_ispy_submissions_email'); ?>
                        <?php if($email4 != '') { ?>
                        <label><img src="<?php echo get_template_directory_uri(); ?>/images/ic_contact us_email.png"> 
                            <a href="mailto:<?php echo CFS()->get('editorial_and_ispy_submissions_email'); ?>">
                            <?php echo CFS()->get('editorial_and_ispy_submissions_email'); ?>
                            </a>
                        </label>
                        <?php } ?>
                    </div>
                </div>
                <div class="rightside">
                    <div class="contatform">


                        <?php echo do_shortcode('[contact-form-7 id="131" title="Contact form 1"]'); ?>

                        <!--
                        <div class="formfields">
                            <div class="field">
                                <label>Email Address</label>
                                <input type="text" name="" placeholder="Type your email">
                            </div>
                            <div class="field">
                                <label>Name</label>
                                <input type="text" name="" placeholder="Type your name">
                            </div>
                        </div>
                        <div class="formfields">
                            <label>Message</label>
                            <textarea placeholder="Type here"></textarea>
                        </div>
                        <div class="button">
                            <input type="submit" name="" value="Submit">
                        </div>
                        -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
?>
