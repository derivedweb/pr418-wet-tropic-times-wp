<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="termsprivacy">
    <div class="sitecontainer">
        <div class="content">
            <h1><?php echo the_title(); ?></h1>

            <?php the_content(); ?>
            
        </div>
    </div>
</div>

<?php endwhile; ?>
	

<?php
get_footer();
