<?php
/**
Template Name: Local Distributor
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>


<div class="storyteamtabs text-center">
    <div class="sitecontainer">
        <ul>
            <li><a href="<?php echo home_url('/'); ?>our-story/">Our Story</a></li>
            <li><a href="<?php echo home_url('/'); ?>our-team/">Meet The Team</a></li>
            <li class="active"><a href="<?php echo home_url('/'); ?>local-distributor/">Distribution List</a></li>
        </ul>
    </div>
</div>

<div class="localdistributers">

    <div class="directorymap">
         <div id="dvMap"></div>
    </div>

    <div class="directorymaplisting">
        <div class="sitecontainer">
            <h1>Find Us at <br> Your Local Distributor</h1>
            <div class="distributerlisting">

                <div class="searchdirectory">
                    <div class="searchbox">
                        <input id="search-input" type="text" placeholder="Enter a location">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_location_search.png">
						<div id="location-error"></div>
                    </div>
                </div>
                <div class="distributeritems">
                    <div class="distributeritemsinner">
                        <?php
                        query_posts(array(
                            'post_type' => 'local_distributor',
                            'posts_per_page' => -1,
                        ));
                        ?>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="item">
                            <div class="image">
                                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                                <a href="<?php echo the_permalink(); ?>">
                                <img src="<?php echo $featured_img_url; ?>">
                                </a>
                            </div>
                            <div class="content">
                                <h3><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
                                <h4><?php echo CFS()->get('address'); ?></h4>
                                <h5><a target="_blank" href="<?php echo CFS()->get('website'); ?>">Website</a></h5>
                                <div class="info">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic_distributor_phone.png">
                                    <label><a href="tel:<?php echo CFS()->get('phone'); ?>"><?php echo CFS()->get('phone'); ?></a></label>
                                    <?php $statuses = CFS()->get( 'status' );
                                    foreach ( $statuses as $key => $label ) {
                                        $status = $label;
                                    }    
                                    ?>
                                    <?php if($status == 'Open') { ?>
                                    <span>
                                        <b>
                                            <?php echo $status; ?>
                                        </b>
                                        &nbsp;
                                        <b>Closes <?php echo CFS()->get('closes'); ?></b>
                                    </span>
                                    <?php } else { ?>
                                        <b style="color: #B72B2B;">
                                            <?php echo $status; ?>
                                        </b>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>

                    </div>
                </div>
                
            </div>
        </div>
    </div>

</div>


<?php
get_footer();
?>

<!--
<script>
	function initMap() {
		var centerCoordinates = new google.maps.LatLng(37.6, -95.665);
		var map = new google.maps.Map(document.getElementById('dvMap'), {
			center : centerCoordinates,
			zoom : 4
		});
		var card = document.getElementById('pac-card');
		var input = document.getElementById('search-input');
		var infowindowContent = document.getElementById('infowindow-content');

		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

		var autocomplete = new google.maps.places.Autocomplete(input);
		var infowindow = new google.maps.InfoWindow();
		infowindow.setContent(infowindowContent);

		var marker = new google.maps.Marker({
			map : map
		});

		autocomplete.addListener('place_changed',function() {
			document.getElementById("location-error").style.display = 'none';
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				document.getElementById("location-error").style.display = 'inline-block';
				document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
				return;
			}

			map.fitBounds(place.geometry.viewport);
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			infowindowContent.children['place-icon'].src = place.icon;
			infowindowContent.children['place-name'].textContent = place.name;
			infowindowContent.children['place-address'].textContent = input.value;
			infowindow.open(map, marker);
		});
	}
</script>
-->



<script type="text/javascript">
        var markers = [

        <?php
        query_posts(array(
            'post_type' => 'local_distributor',
            'posts_per_page' => -1,
        ));
        if (have_posts()) :
            while (have_posts()) : the_post(); 
            $featured_img = get_the_post_thumbnail_url(get_the_ID(), 'full');

            $statuses = CFS()->get( 'status' );
            foreach ( $statuses as $key => $label ) {
                $status = $label;
            }    
        ?>

        {
            "title": '<?php echo the_title(); ?>',
            "lat": '<?php echo CFS()->get('latitude'); ?>',
            "lng": '<?php echo CFS()->get('longitude'); ?>',
            "description": '<img src="<?= $featured_img; ?>"><h3><?php echo the_title(); ?></h3><h4><?php echo CFS()->get('address'); ?></h4><div class="phone"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_distributor_phone.png"><label><?php echo CFS()->get('phone'); ?></label></div><?php if($status == 'Open') { ?><span>Open <b>Closes <?php echo CFS()->get('closes'); ?></b></span><?php } else { ?> <span class="red">Closed</span> <?php } ?> '
        },
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>

        
        ];
        window.onload = function () {
            initMap();
        }
        function initMap() {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
     		//Create and open InfoWindow.
            var infoWindow = new google.maps.InfoWindow();
			/*
			  	var card = document.getElementById('pac-card');
				var infowindowContent = document.getElementById('infowindow-content');
				map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

				var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(infowindowContent);
			*/
			// NEW CODE ADDED
			var input = document.getElementById('search-input');
			var autocomplete = new google.maps.places.Autocomplete(input);

			var locationMarker = new google.maps.Marker({
				map : map
			});
			autocomplete.addListener('place_changed',function() {
				document.getElementById("location-error").style.display = 'none';
				infoWindow.close();
				locationMarker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					document.getElementById("location-error").style.display = 'inline-block';
					document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
					return;
				}

				map.fitBounds(place.geometry.viewport);
				locationMarker.setPosition(place.geometry.location);
				locationMarker.setVisible(false);

				//infowindowContent.children['place-icon'].src = place.icon;
				//infowindowContent.children['place-name'].textContent = place.name;
				//infowindowContent.children['place-address'].textContent = input.value;
				//infoWindow.open(map, locationMarker);
			});
			// NEW CODE END
     
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title,
                    icon: '<?php echo get_template_directory_uri(); ?>/images/ic_map_pin.png'
                });
     
                //Attach click event to the marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                        infoWindow.setContent("<div class='dirbox'>" + data.description + "</div>");
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&signed_in=true&libraries=places&callback=initMap"
        async defer></script>
