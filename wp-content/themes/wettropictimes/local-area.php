<?php
/**
Template Name: Local Area
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<?php $post_id = get_the_ID(); ?>

<div class="categorybanner">
    <div class="layer"></div>
    <?php $featured_img_url = get_the_post_thumbnail_url($post_id, 'full'); ?>
    <img src="<?php echo $featured_img_url; ?>">
    <div class="heading">
        <span>Wet Tropic Times</span>
        <h1>Our Local Areas</h1>
    </div>
</div>

<div class="localarealisting">
    <div class="sitecontainer">
        <div class="localarealistingitems">

            <?php
            query_posts(array(
                'post_type' => 'local_area',
                'posts_per_page' => -1,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <div class="image">
                            <div class="layer"></div>
                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                            <img class="mainimg" src="<?php echo $featured_img_url; ?>">
                            <h3> <?php echo the_title(); ?></h3>
                            <img class="arrow" src="<?php echo get_template_directory_uri(); ?>/images/ic_local_area_arrow.png">
                        </div>
                        <div class="content">
                            <?php 
                            $big = wp_trim_words(get_the_content(), 15);
                            $small = substr($big, 0, 80);
                            ?>
                            <p><?php echo $small; ?>...</p>
                            
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>
    </div>
</div>


<?php
get_footer();
?>

