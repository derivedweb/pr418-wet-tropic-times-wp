<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<?php global $wet_vars; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>

	<!--Main Stylesheet-->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
   
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" />
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&libraries=places"></script>
    
</head>
<body <?php body_class(); ?> onload="initMap();">
<?php wp_body_open(); ?>




<?php
// PHP code to extract IP 
  
function getVisIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
        return $_SERVER['REMOTE_ADDR'];
    }
}
$vis_ip = getVisIPAddr();
$ipdat = @json_decode(file_get_contents(
    "http://www.geoplugin.net/json.gp?ip=" . $vis_ip));

/*   
echo 'Country Name: ' . $ipdat->geoplugin_countryName . "\n";
echo 'City Name: ' . $ipdat->geoplugin_city . "\n";
echo 'Continent Name: ' . $ipdat->geoplugin_continentName . "\n";
echo 'Latitude: ' . $ipdat->geoplugin_latitude . "\n";
echo 'Longitude: ' . $ipdat->geoplugin_longitude . "\n";
echo 'Currency Symbol: ' . $ipdat->geoplugin_currencySymbol . "\n";
echo 'Currency Code: ' . $ipdat->geoplugin_currencyCode . "\n";
echo 'Timezone: ' . $ipdat->geoplugin_timezone;
*/
	
?>

<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <div class="login">
        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_login.png"> Login</a>
    </div>

    <?php 
        wp_nav_menu(array(
            'theme_location' => 'header',
            'menu_class' => 'navmenu',
        ));
    ?> 

    <div class="downloadapp">
        <label>Download app</label> 
        <?php if (!empty($wet_vars['apple_store_url'])) : ?>
        <a href="<?php echo $wet_vars['apple_store_url']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_apple.png"></a>
        <?php endif; ?>
        <?php if (!empty($wet_vars['play_store_url'])) : ?>
        <a href="<?php echo $wet_vars['play_store_url']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_play_store.png"></a>
        <?php endif; ?>
    </div>
</div>

<div class="header">
    
    <div class="headertop">
        <div class="leftside">
            <div class="contact">
                <a href="<?php echo home_url(); ?>/contact-us"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_contactus.png"> Contact Us</a>
            </div>
            <div class="social">
                <ul>
                	<?php if (!empty($wet_vars['fb_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['fb_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_facebook.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($wet_vars['insta_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['insta_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_instagram.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($wet_vars['twit_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['twit_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_twitter.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($wet_vars['linked_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['linked_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_linkedin.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>  
            <div class="downloadapp">
                <label>Download app</label> 
                <?php if (!empty($wet_vars['apple_store_url'])) : ?>
                <a href="<?php echo $wet_vars['apple_store_url']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_apple.png"></a>
                <?php endif; ?>
                <?php if (!empty($wet_vars['play_store_url'])) : ?>
                <a href="<?php echo $wet_vars['play_store_url']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_play_store.png"></a>
                <?php endif; ?>
            </div>  
        </div>
        <div class="rightside">
            <div class="address">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_location.png">
                <label>
					<span class="cityname"></span> 
					<input type="text" id="pac-input"/>
					<br> 
					<a href="#">Change</a>
				</label>
    			<div id="map" style="display: none;"></div>
            </div>
            <div class="weather">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_weather.png">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
						<span class="maxtemp"><?php //echo $apiData['current_observation']->condition->temperature; ?></span>
						C Max <br> 
						Min of <span class="mintemp">15</span>
                    </button>
                </div>
            </div>
            <div class="login">
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_login.png"> Login</a>
            </div>
        </div>
    </div>
    <div class="headermiddle">
        <span class="togglesidemenu" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>
        <div class="sitecontainer">
        	<?php if (!empty($wet_vars['site_logo'])) : ?>
            <div class="logo">
                <a href="<?= home_url(); ?>"><img src="<?php echo $wet_vars['site_logo']; ?>"></a>
            </div>
            <?php endif; ?>

            <div class="menus">
                <?php 
			        wp_nav_menu(array(
			            'theme_location' => 'header',
			            'menu_class' => 'text-center',
			        ));
			    ?> 
            </div>
            <div class="search">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_search.png">
            </div>
        </div>
    </div>

    <div class="headerbottom">
        <div class="sitecontainer">
            <div class="categorymenus">

            	<?php 
			        wp_nav_menu(array(
			            'theme_location' => 'header_categories',
			            'menu_class' => 'desktopcategorymenus',
			        ));
			    ?> 

                <div class="mobilecategorymenus dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Categories
                    <span class="caret"></span></button>

                    <?php 
				        wp_nav_menu(array(
				            'theme_location' => 'header_categories',
				            'menu_class' => 'dropdown-menu',
				        ));
				    ?> 
                </div>
            </div>
        </div>
    </div>

</div>