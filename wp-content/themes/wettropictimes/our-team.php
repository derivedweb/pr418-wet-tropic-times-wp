<?php
/**
Template Name: Our Team
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>


<div class="storyteamtabs text-center">
    <div class="sitecontainer">
        <ul>
            <li><a href="<?php echo home_url('/'); ?>our-story/">Our Story</a></li>
            <li class="active"><a href="<?php echo home_url('/'); ?>our-team/">Meet The Team</a></li>
            <li><a href="<?php echo home_url('/'); ?>local-distributor/">Distribution List</a></li>
        </ul>
    </div>
</div>

<div class="ourstoryteams">
    <div class="sitecontainer">
        <h1>Our Team</h1>
        
        <div class="ourteamitems">

            <?php $fields = CFS()->get('our_team_items'); ?>
            <?php foreach ($fields as $field) { ?>
            <div class="item">
                <div class="iteminner">
                    <div class="image">
                        <img src="<?php echo $field['our_team_image']; ?>">
                    </div>
                    <div class="content">
                        <h3>
                            <?php echo $field['our_team_name']; ?> 
                            <span><?php echo $field['our_team_designation']; ?></span>
                            <a href="mailto:<?php echo $field['our_team_email_id']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_team_profile_contact.png"></a>
                        </h3>
                        <p>
                            <?php echo $field['our_team_description']; ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php } ?>
            
        </div>
    </div>
</div>


<?php
get_footer();
?>
