$(document).ready(function(){
  
	  get_location_info_by_ip();
});
				  
				  
function get_location_info_by_ip(){
    
  jQuery.ajax({
    url: '//hero-pet.de/ajax/get_location_info_by_ip.php',
    type: 'get',
    dataType: 'json',
    success: function(data) {
      
      var latitude = data.latitude;
      var longitude = data.longitude;
     
	  $('#pet_country').val(data.country_name);
      set_filter_map_data(latitude, longitude);
              
  }
 });  
}

function set_filter_map_data(latitude, longitude)
{
      var a = [];
      var infoBubble = [];
      var markers = [];
      var data = $('#pet_filter_form').serialize();

      var mapCenter = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
            zoom: 5,
            center: mapCenter,zoomControl: true,
            zoomControlOptions: {
              position: google.maps.ControlPosition.LEFT_CENTER
            },streetViewControl: true,
            streetViewControlOptions: {
              position: google.maps.ControlPosition.LEFT_TOP
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,minZoom: 3,scrollwheel: false
          };
    
      var map = new google.maps.Map(document.getElementById("map"), mapOptions);
      var infowindow = new google.maps.InfoWindow();

      $.ajax({
       type:'GET',
       async:true,
       dataType: "json",
       data:  {action : 'get_filter_map_data', search_data:data},
       url:'//hero-pet.de/ajax/getmapdata.php',
       success: function(data)
       {
         
           $.each( data, function(i, value) {
			   
		   
           var myLatlng = new google.maps.LatLng(value.latitude, value.longitude);
           var y=value.pet_lost_date.split("/");

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
          
            var yyyy = today.getFullYear();
            if(dd<10){
              dd='0'+dd
            }
            if(mm<10){
              mm='0'+mm
            }

           var today = dd+'/'+mm+'/'+yyyy;

           var date2 = new Date(yyyy, mm, dd);
           var date1 = new Date(y[2],y[1], y[0]);
           
           var one_day = 1000*60*60*24; //Get 1 day in milliseconds


           var days = Math.ceil( (date2.getTime() - date1.getTime() ) / one_day);
           
           /*marker = new google.maps.Marker({
             position: myLatlng,
             map: map,
             icon: 'http://hero-pet.com/'+value.icon,
           });*/
 
           var marker = new google.maps.Marker({
              position: myLatlng,icon: value.icon,
              map:map
            });
           
           //map.setCenter(marker.getPosition());
			   
			 a[i] = '<div class="listing-box '+value.ribbon+'" id="mapbox" style="background: white;"><div class="shadow-box"><h3 class="title" style="font-size:20px;">'+value.pet_name+'</h3><p class="subtitle">'+value.pet_breed+'</p><div class="image"><a href="//hero-pet.de/index.php?id=33&petID='+value.id+'" id="listinglink" target="_blank"><img src="'+value.img+'" class="img-responsive" id="centerimg"></a></div><div class="quick-info clearfix"><div class="left"><p><span class="icon" style="font-size: 12px;"><img src="//hero-pet.de/fileadmin/templates/images/location.png" alt=""></span>'+value.pet_postal+' '+value.pet_city+', '+value.pet_country+'</p><p><span class="icon"><img src="//hero-pet.de/fileadmin/templates/images/clock.png" alt=""></span> '+value.date_of_founding+'</p></div><div class="right"><p><a href="//hero-pet.de/index.php?id=33&petID='+value.id+'">'+value.link+'</a></p></div></div></div><div>';    
			   
            
          /* if(value.showtel == 'Yes')
           {
             a[i] = '<div class="listing-box '+value.ribbon+'" id="mapbox" style="background: white;"><div class="shadow-box" ><h3 class="title" style="font-size:20px;">'+value.pet_name+'</h3><p class="subtitle">'+value.pet_breed+'</p><div class="image"><a href="index.php/'+value.id+'?id=33" id="listinglink" target="_blank"><img src="'+value.img+'" class="img-responsive" id="centerimg"></a></div><div class="quick-info clearfix"><div class="left"><p><span class="icon" style="font-size: 12px;"><img src="//hero-pet.de/fileadmin/templates/images/location.png" alt=""></span>'+value.pet_postal+' '+value.pet_city+', '+value.pet_country+'</p><p><span class="icon"><img src="//hero-pet.de/fileadmin/templates/images/phone.png" alt=""></span> '+value.phoneno+'</p><p id="rewardon"><span class="icon"><img src="//hero-pet.de/fileadmin/templates/images/reward.png" alt=""></span> Reward:'+value.pet_reward+''+value.currency+' </p></div><div class="right"><p style="margin-top: 14px;"><img src="//hero-pet.de/fileadmin/templates/images/mini_logo.png" style="width:95px;" alt=""></p><p>'+days+' Days Ago</p></div></div></div><div>';
           }
           else
           {
             a[i] = '<div class="listing-box '+value.ribbon+'" id="mapbox" style="background: white;"><div class="shadow-box"><h3 class="title" style="font-size:20px;">'+value.pet_name+'</h3><p class="subtitle">'+value.pet_breed+'</p><div class="image"><a href="index.php/'+value.id+'?id=33" id="listinglink" target="_blank"><img src="'+value.img+'" class="img-responsive" id="centerimg"></a></div><div class="quick-info clearfix"><div class="left"><p><span class="icon" style="font-size: 12px;"><img src="//hero-pet.de/fileadmin/templates/images/location.png" alt=""></span>'+value.pet_postal+' '+value.pet_city+', '+value.pet_country+'</p><p><span class="icon"></span></p><p id="rewardon"><span class="icon"><img src="//hero-pet.de/fileadmin/templates/images/reward.png" alt=""></span> Reward:'+value.pet_reward+''+value.currency+'</p></div><div class="right"><p style="margin-top: 14px;"><img src="//hero-pet.de/fileadmin/templates/images/mini_logo.png" style="width:95px;" alt=""></p><p>'+days+' Days Ago</p></div></div></div><div>';
           }
           */
           infoBubble[i] = new InfoBubble({
             disableAutoPan: false,padding: 0,borderWidth:0,shadowStyle: 2,shadowStyle: 0,
           }); 
           
           var backgroundColor = "transparent";
           
           infoBubble[i].setContent(a[i]);
           
           google.maps.event.addListener(marker, 'click', (function(marker, i) {
             
             return function() {
               if (!infoBubble[i].isOpen())
               {
                 for(var j=1;j<=value.count;j++)
                 {
                   if(j!=i)
                   {
                     if( infoBubble[j] != null ) { infoBubble[j].close(); }
                   }
                 }
                 infoBubble[i].setBackgroundColor(backgroundColor);
                 infoBubble[i].setBorderColor(backgroundColor);
                 infoBubble[i].open(map, marker);
               }
               else
               {
                 infoBubble[i].close();
               }
               
             }
            })(marker, i));
            
            markers.push(marker);
      
           //map.setCenter(marker.getPosition());
           
         });
         
         //var markerCluster = new MarkerClusterer(map, markers);
        /* google.maps.event.addDomListener(window, "resize", function() {
           var center = map.getCenter();
           google.maps.event.trigger(map, "resize");
           map.setCenter(center);
         }); */
         
       }
       
      });
}