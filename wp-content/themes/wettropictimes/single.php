<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>


<div class="categorydetailbanner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img_article_header.png);">
	<div class="layer"></div>
	<div class="sitecontainer">
	    <div class="breadcrumbs">
	    	<?php $cats = get_the_category(get_the_ID()); ?>
	        <a href="#">News /</a> <a href="<?php echo home_url(); ?>/back_end/category/<?php echo $cats[0]->slug; ?>"><?php echo $cats[0]->name; ?></a> / All region top stories
	    </div>
	    <h1><?php echo the_title(); ?></h1>
	</div>
</div>

<div class="articledetails">
    <div class="sitecontainer">
        <div class="leftside">

        	<?php while ( have_posts() ) : the_post(); ?>
	            <div class="topinfo">

                    <?php
                    $post_id = get_the_ID();
                    $author_id = get_post_field( 'post_author', $post_id )
                    ?>
                    <img src="<?php echo esc_url( get_avatar_url($author_id) ); ?>" />
	                
	                By
                    <label><?php echo CFS()->get('author_name'); ?></label> 
                    <!--
                    <label><?php echo get_the_author_meta( 'nicename', $author_id ); ?></label> 
                    -->
	                <span><?php echo get_the_date(); ?></span>
	                <div class="share">
	                	<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
	                    <a class="a2a_button_facebook" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_news_share_facebook.png"></a>
	                    <a class="a2a_button_twitter" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_news_share_instagram.png"></a>
	                    <a class="a2a_dd" href="https://www.addtoany.com/share"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_news_share_share.png"></a>

	                	</div>
	                    <script async src="https://static.addtoany.com/menu/page.js"></script>
	                </div>

	            </div>

	            <?php if ( !in_category('Videos') ) { ?> 
	            <?php if ( !in_category('Podcasts') ) { ?>  	
	            <div class="imageslider">
	                <div class="mainimage">
	                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
	                    <img src="<?php echo $featured_img_url; ?>"> 
	                    <span class="imgcaption"><?php echo CFS()->get('image_caption'); ?></span>
	                </div>

                    <?php $fields = CFS()->get('gallery_images'); ?>
                    <?php if (!empty($fields)) { ?>
	                <div class="galleryimg text-center">
	                    <button type="button" class="top-arrow" style="display: inline-block;">
	                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_images_arrow_up.png">
	                    </button>
	                     <div class="slick-carousel">
	                     	<div class="item">
	                            <img src="<?php echo $featured_img_url; ?>">
	                        </div>
                           <?php if (is_array($fields) || is_object($fields)) { ?>
    					   <?php foreach ($fields as $field) { ?>
	                       <div class="item">
	                            <img src="<?php echo $field['gallery_image']; ?>">
	                       </div>
	                       <?php } } ?>
	                    </div>
	                    <button type="button" data-role="none" class="bottom-arrow" aria-label="Next" role="button" style="display: inline-block;" aria-disabled="false">
	                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_images_arrow_down.png">
	                    </button>
	                </div>
                    <?php } ?>
	            </div>
	            <?php } } ?>

	            <?php if (in_category('Videos') ) { ?>
	            <?php $video = CFS()->get('upload_video'); ?> 	
	            <?php if($video != ''){ ?>
	            <div class="video">
                    <video width="100%" controls>
                        <source src="<?php echo CFS()->get('upload_video'); ?>" type="video/mp4">
                        <source src="<?php echo CFS()->get('upload_video'); ?>" type="video/ogg">
                        Your browser does not support the video tag.
                    </video> 
                    <span class="imgcaption">Image Caption <br> Goes Here</span>
                </div>
                <?php } } ?>

                <?php if (in_category('Podcasts') ) { ?>
                <?php $audio = CFS()->get('upload_audio'); ?> 	
	            <?php if($audio != ''){ ?>	
                <div class="audio">
                    <audio preload="auto" controls>
                        <source src="<?php echo CFS()->get('upload_audio'); ?>">
                    </audio>
                </div>
                <?php } } ?>

	            <div class="articlecontent">
	                <div class="hidehalf">
	                    <?php the_content(); ?>
	                </div>

	                <div class="loadmorestory">
	                    <a id="readmore" href="javascript:void(0);">
	                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_stories_load more.png"> <br>
	                        Reads More
	                    </a>
	                </div>
	            </div>
	        <?php endwhile; ?>    

        </div>
        <div class="rightside">
            <h2>Top Stories</h2>
            <div class="topstory">
                <?php
	            query_posts(array(
	                'post_type' => 'post',
	                'posts_per_page' => 6,
	                'meta_key' => 'top_stories',
	                'meta_value' => 1
	            ));
	            ?>
	            <?php if (have_posts()) : ?>
	            <?php while (have_posts()) : the_post(); ?>
                <div class="item">
                	<a href="<?php echo the_permalink(); ?>">
                	<?php $author_id = get_the_author_meta(get_the_ID()); ?>
	                <label>By <?php echo CFS()->get('author_name'); ?></label> 
                    <span>Posted <?php echo get_the_date(); ?></span>
                    <h3><?php echo the_title(); ?></h3>
                    <?php 
						$preview = CFS()->get('preview_text');
						$big = wp_trim_words($preview, 50);
						$small = substr($big, 0, 150);
						?>
						<p><?php echo $small; ?></p>
                	</a>
                </div>
                <?php endwhile; ?>
	            <?php endif; ?>
	            <?php wp_reset_query(); ?>

            </div>
        </div>
    </div>
</div>


<div class="categorywisestory">
    <div class="sitecontainer">
        <div class="heading">
            <h3>More News</h3>
        </div>
        <div class="storieslisting">

            <?php
            query_posts(array(
                'post_type' => 'post',
                'posts_per_page' => 4,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'medium_large'); ?>
                        <img class="featureimg" src="<?php echo $featured_img_url; ?>">
                        <div class="content">
                            <span>Posted <?php echo get_the_date(); ?></span>
                            <h3><?php echo wp_trim_words(get_the_title(), 100); ?></h3>
                            <?php 
							$preview = CFS()->get('preview_text');
							$big = wp_trim_words($preview, 50);
							$small = substr($big, 0, 65);
							?>
							<p><?php echo $small; ?></p>
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</div>

<?php
get_footer(); ?>


<script>
    $("#readmore").on("click", function(){
        $(".articlecontent .hidehalf").css('height','auto');
        $(".articledetails .loadmorestory").hide();
    });
</script>

<script>
    $('.slick-carousel').slick({
      infinite: true,
      vertical:true,
      verticalSwiping:true,
      slidesToShow: 3,
      slidesToScroll: 3,
      prevArrow: $('.top-arrow'),
      nextArrow: $('.bottom-arrow')
    });
</script>

<script>
    $(".articledetails .imageslider .galleryimg .item img").on("click", function(){
        var src = $(this).attr('src');
        $(".articledetails .imageslider .mainimage img").attr('src',src);
    });
</script>
