<?php
/**
Template Name: Our Story
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<div class="categorybanner">
    <div class="layer"></div>
    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
    <img src="<?php echo $featured_img_url; ?>">
    <div class="heading">
        <h1>About Us</h1>
    </div>
</div>

<div class="storyteamtabs text-center">
    <div class="sitecontainer">
        <ul>
            <li class="active"><a href="<?php echo home_url('/'); ?>our-story/">Our Story</a></li>
            <!--<li><a href="<?php echo home_url('/'); ?>our-team/">Meet The Team</a></li>-->
            <li><a href="<?php echo home_url('/'); ?>local-distributor/">Distribution List</a></li>
			<li><a href="<?php echo home_url('/'); ?>back_end/wet-tropics-church-services">Church Services</a></li>
        </ul>
    </div>
</div>

<div class="ourstoryteams">
    <div class="sitecontainer">
        <h1><?php echo the_title(); ?></h1>
        <div class="content">
            <?php echo the_content(); ?>
        </div>

        <?php $fields = CFS()->get('our_story_images'); ?>
        <?php if (is_array($fields) || is_object($fields) || !empty($fields)) { ?>
        <div class="ourstoryslider">
            <div class="owl-carousel owl-ourstory owl-theme">
                <?php foreach ($fields as $field) { ?>
                <div class="item">
                    <img src="<?php echo $field['story_image']; ?>">
                </div>
                <?php } ?>
            </div>
        </div>
        <?php } ?>

    </div>
</div>


<div class="ourstoryteams">
    <div class="sitecontainer">
        <h1>Our Team</h1>
        
        <div class="ourteamitems">

            <?php $fields = CFS()->get('our_team_items'); ?>
            <?php foreach ($fields as $field) { ?>
            <div class="item">
                <div class="iteminner">
                    <div class="image">
                        <img src="<?php echo $field['our_team_image']; ?>">
                    </div>
                    <div class="content">
                        <h3>
                            <?php echo $field['our_team_name']; ?> 
                            <span><?php echo $field['our_team_designation']; ?></span>
                            <a href="mailto:<?php echo $field['our_team_email_id']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_team_profile_contact.png"></a>
                        </h3>
                        <p>
                            <?php echo $field['our_team_description']; ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php } ?>
            
        </div>
    </div>
</div>

<?php
get_footer();
?>

<script>
    var owl = $('.owl-ourstory');
    owl.owlCarousel({
        loop:true,
        nav:true, 
        margin:20,
        autoplay:true,
        dots: true,
        items:4,
        responsive:{
           0:{
               items:2
           },
           700:{
               items:3
           },            
           991:{
               items:4
           },
           1200:{
               items:4 // Row in content item set
           }
       }
    });
    $( ".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $( ".owl-next").html('<i class="fa fa-angle-right"></i>');
</script>