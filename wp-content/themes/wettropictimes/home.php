<?php
/**
Template Name: Home
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<div class="heroslider">

    <div class="sitecontainer">
        <div class="slideritems">

            <div class="owl-carousel owl-herobanner owl-theme">
                
                <?php $values = CFS()->get( 'banner_post' ); ?>
                <?php foreach ( $values as $post_id ) { ?>
                <?php $the_post = get_post( $post_id ); ?> 
                
                <div class="item">
                    <div class="layer"></div>
                    <?php $featured_img_url = get_the_post_thumbnail_url($post_id, 'full'); ?>
                    <img src="<?php echo $featured_img_url; ?>">
                    <div class="content">
                        <div class="possted">
                            <?php $cats = get_the_category($post_id); ?>
                            <a href="#"><?php echo $cats[0]->name; ?></a> 
                            <p>Posted <?php echo get_the_date(); ?></p>
                        </div>
                        <h2><?php echo $the_post->post_title; ?></h2>
                        
                        <h4><?php echo $the_post->tagline; ?></h4>
                        <h5><?php echo wp_trim_words($the_post->preview_text, 30); ?></h5>
                        <a class="viewstory" href="<?php echo get_permalink($post_id); ?>">View Story</a>
                    </div>
                </div>
                <?php } ?>
                
            </div>
        </div>

        <div class="sliderdots dots">
            <div id='carousel-custom-dots' class='owl-dots'>
                <?php $values1 = CFS()->get( 'banner_post' ); ?>
                <?php foreach ( $values1 as $post_id1 ) { ?>
                <?php $the_post1 = get_post( $post_id1 ); ?> 

                <div class="item owl-dot active">
                    <a class="slide1" href="javascript:void(0);">
                        <div class="img">
                            <?php $imgid = $the_post1->thumbnail_image; ?>
                            <?php echo wp_get_attachment_image($imgid); ?>
                        </div>
                        <div class="info">
                            <span>Posted <?php echo get_the_date(); ?></span>
                            <h4><?php echo $the_post1->post_title; ?></h4>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>	


<div class="flightticketbook">
    <div class="sitecontainer">
        <a href="<?php echo CFS()->get('advertis_image_url'); ?>">
            <img src="<?php echo CFS()->get('advertis_image'); ?>">
        </a>
    </div>
</div>



<div class="regiontopstores">
    <div class="sitecontainer">

        <div class="topstoryhead">
            <div class="selectbox">
                <h2>All Regions Top Stories</h2>
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>

        <div class="storieslisting">

            <?php
            query_posts(array(
                'post_type' => 'post',
                'posts_per_page' => -1,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'medium_large'); ?>
                        <img class="featureimg" src="<?php echo $featured_img_url; ?>">
                        <div class="content">
                            <span>Posted <?php echo get_the_date(); ?></span>
                            <?php 
                            $title = wp_trim_words(get_the_title(), 100);
                            $titlesmall = substr($title, 0, 500);
                            ?>
                            <h3><?php echo $titlesmall; ?></h3>
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>

        <div class="loadmorestory">
            <a id="loadMore" href="javascript:void(0);">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ic_stories_load more.png"> <br>
                Load More Stories
            </a>
        </div>

    </div>
</div>


<div class="categorywisestory videos">
    <div class="sitecontainer">
        <div class="heading">
            <h3>Videos</h3>
            <a href="<?php echo home_url('/'); ?>back_end/category/videos/">More Videos</a>
        </div>
        <div class="storieslisting">
            <?php
            query_posts(array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'cat'         => 10,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <div class="image">
                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                            <img class="featureimg" src="<?php echo $featured_img_url; ?>">
                            <a class="play" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_story_video_play.png"> 3:56</a>
                        </div>
                        <div class="content">
                            <span>Posted <?php echo get_the_date(); ?></span>
                            <?php 
                            $title = wp_trim_words(get_the_title(), 100);
                            $titlesmall = substr($title, 0, 500);
                            ?>
                            <h3><?php echo $titlesmall; ?></h3>
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>
    </div>
</div>



<div class="categorywisestory">
    <div class="sitecontainer">
        <div class="heading">
            <h3>Community</h3>
            <a href="<?php echo home_url('/'); ?>back_end/category/community/">More Community</a>
        </div>
        <div class="storieslisting">
            
            <?php
            query_posts(array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'cat'         => 2,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <div class="content">
                            <span>Posted <?php echo get_the_date(); ?></span>
                            <?php 
                            $title = wp_trim_words(get_the_title(), 100);
                            $titlesmall = substr($title, 0, 500);
                            ?>
                            <h3><?php echo $titlesmall; ?></h3>
                            <?php 
							$preview = CFS()->get('preview_text');
                            $big = wp_trim_words($preview, 50);
                            $small = substr($big, 0, 100);
                            ?>
                            <p><?php echo $small; ?></p>
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>
    </div>
</div>

<div class="categorywisestory">
    <div class="sitecontainer">
        <div class="heading">
            <h3>Sports</h3>
            <a href="<?php echo home_url('/'); ?>back_end/category/rural/">More Sports</a>
        </div>
        <div class="storieslisting">
            <?php
            query_posts(array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'cat'         => 4,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <div class="content">
                            <span>Posted <?php echo get_the_date(); ?></span>
                            <?php 
                            $title = wp_trim_words(get_the_title(), 100);
                            $titlesmall = substr($title, 0, 500);
                            ?>
                            <h3><?php echo $titlesmall; ?></h3>
                            <?php 
							$preview = CFS()->get('preview_text');
                            $big = wp_trim_words($preview, 50);
                            $small = substr($big, 0, 100);
                            ?>
                            <p><?php echo $small; ?></p>
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            
        </div>
    </div>
</div>


<?php
get_footer();
?>

<script>
    var owl = $('.owl-herobanner');
    owl.owlCarousel({
        loop:true,
        nav:false, 
        margin:0,
        autoplay:false,
        dots: false,
        items:1,
        touchDrag : false,
        mouseDrag : false
    });
    
    $('.owl-dot').click(function () {
         var owl = $(".owl-herobanner");
         owl.trigger('to.owl.carousel', [$(this).index(), 300]);

         $('.heroslider .sliderdots .item.active').removeClass('active');
         $(this).addClass('active');
    });
</script>

<script>
    $(document).ready(function(){
      $(".regiontopstores .storieslisting .item").slice(0, 8).show();
      $("#loadMore").on("click", function(e){
        e.preventDefault();
        $(".regiontopstores .storieslisting .item:hidden").slice(0, 12).slideDown();
        if($(".regiontopstores .storieslisting .item:hidden").length == 0) {
          $("#loadMore").text("").addClass("noContent");
        }
      });
    })
</script>

<script>
    $(window).scroll(function(){
      var sticky = $('.header'),
          scroll = $(window).scrollTop();

      if (scroll >= 1) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
</script>
