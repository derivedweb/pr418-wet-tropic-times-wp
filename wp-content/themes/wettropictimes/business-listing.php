<?php
/**
Template Name: Business Listing
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<div class="businesslisting">
    <div class="businessbanner">
        <div class="image">
            <img src="<?php echo CFS()->get('banner_image'); ?>">
        </div>
        <div class="content">
            <h2><?php echo CFS()->get('banner_title'); ?></h2>
            <p><?php echo CFS()->get('banner_description'); ?></p>
            <div class="buttons">
                <a href="#">List Your Business</a>
                <a class="contact" href="<?php echo home_url('/'); ?>contact-us/">Contact Us</a>
            </div>
        </div>
    </div>

    <div class="drivequality" style="background-image: url('<?php echo CFS()->get('drive_quality_bg_image'); ?>');">
        <div class="drivequalityinner">
            <div class="content text-center">
                <h2><?php echo CFS()->get('drive_quality_title'); ?></h2>
                <p><?php echo CFS()->get('drive_quality_tagline'); ?></p>
                <div class="buttons">
                    <a href="#">List Your Business</a>
                    <a class="contact" href="<?php echo home_url('/'); ?>contact-us/">Contact Us</a>
                </div>
                <a class="viewadd" href="#">View Our Advertising Options</a>
            </div>
        </div>
    </div>

    <div class="budgetplans">
        <div class="sitecontainer">
            <h2><?php echo CFS()->get('plans_to_suit_title'); ?></h2>
            <div class="budgetplansbox">

                <?php $fields = CFS()->get('business_plan_items'); ?>
                <?php foreach ($fields as $field) { ?>
                <div class="item text-center">
                    <div class="iteminner" style="background-image: url('<?php echo $field['plan_image']; ?>');">
                        <h3><?php echo $field['plan_title']; ?></h3>
                        <span><?php echo $field['plan_price']; ?></span>
                        <p><?php echo $field['plan_tagline']; ?></p>
                        <ul>
                            <?php foreach ($field['plan_informations'] as $colors => $label) { ?>
                                <li><?php echo $label['plan_info_item']; ?></li>
                            <?php } ?>
                        </ul>
                        <div class="button">
                            <a href="<?php echo $field['plan_button_url']; ?>">Let’s get started</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <div class="businesscontactus">
        <div class="sitecontainer">
            <div class="heading">
                <h2><?php echo CFS()->get('contact_title'); ?></h2>
                <p><?php echo CFS()->get('contact_tagline'); ?></p>
                <div class="contactform">

                    <?php echo do_shortcode('[contact-form-7 id="182" title="Business Contact Form"]'); ?>

                    <!--
                    <div class="formfield">
                        <div class="field">
                            <label>Name</label>
                            <input type="text" name="" placeholder="Type your name">
                        </div>
                        <div class="field">
                            <label>Email Address</label>
                            <input type="text" name="" placeholder="Type your email">
                        </div>
                    </div>
                    <div class="formfield">
                        <label>Message</label>
                        <textarea placeholder="Type here"></textarea>
                    </div>
                    <div class="button">
                        <button type="submit">Submit</button>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
?>
