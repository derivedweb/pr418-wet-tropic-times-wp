<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>


<div class="viewareabreadcrumb">
    <div class="sitecontainer">
        <div class="breadcrumbs">
            <a href="#">Local Areas /</a> <?= the_title(); ?>
        </div>
        <h1><?= the_title(); ?></h1>
        <a data-toggle="modal" data-target="#viewareavideo" class="play" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_story_video_play.png"> Play Video</a>
    </div>
</div>

<div class="viewareagallery articledetails">
    <div class="sitecontainer">

        <div class="leftside imagecrowsel">
            <div class="imageslider">
                <div class="mainimage">
                    <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
	                <img src="<?php echo $featured_img_url; ?>">
                </div>
                <div class="galleryimg text-center">
                    <button type="button" class="top-arrow" style="display: inline-block;">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_images_arrow_up.png">
                    </button>
                     <div class="slick-carousel">
                     	<?php $images3 = get_field('area_gallery_images'); ?>
    					<?php foreach ($images3 as $image3) { ?>
                       <div class="item">
                            <img src="<?php echo $image3['area_image']; ?>">
                       </div>
                       <?php } ?>
                    </div>

                    <button type="button" data-role="none" class="bottom-arrow" aria-label="Next" role="button" style="display: inline-block;" aria-disabled="false">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_images_arrow_down.png">
                    </button>
                </div>
            </div>
        </div>


        <div class="viewareagalleryitems">
            <div class="itemleft">
                <div class="item">
                    <div class="iteminner">
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
	                	<img src="<?php echo $featured_img_url; ?>">
                    </div>
                </div>
            </div>
            <div class="itemright">
                <?php
                $images = get_field('area_gallery_images');
                $count = count($images);
                $i = 1;
                ?>

                <?php foreach ($images as $image) { ?>
                <?php if($i <= 4) { ?>
                <div class="item">
                    <div class="iteminner">
                    	<?php if($i == 4) { ?>
                        <?php if($count > 4) { ?>    
                        <div class="layer"></div>
                        <label>+<?php echo $count - 4; ?> Photos</label>
                    	<?php } } ?>
                        <img src="<?php echo $image['area_image']; ?>">
                    </div>
                </div>
                <?php } $i++; } ?>

            </div>
        </div>

        <div class="othergalery">
            <?php $images1 = get_field('area_gallery_images'); ?>
            <?php $j = 1; ?>
            <?php foreach ($images1 as $image1) { ?>
            <?php if($j >= 5) { ?> 
            <div class="item">
                <div class="iteminner">
                    <img src="<?php echo $image1['area_image']; ?>">
                </div>
            </div>
            <?php } $j++; } ?>
        </div>


    </div>
</div>

<div class="viewareacontent">
    <div class="sitecontainer">
        <?php the_content(); ?>
    </div>
</div>

<div class="viewareamap">
    <div class="sitecontainer">
        <?php echo the_field('area_map_iframe'); ?>
    </div>
</div>

<div class="localarealisting">
    <div class="sitecontainer">
        <div class="localarealistingitems">
            <?php
            query_posts(array(
                'post_type' => 'local_area',
                'posts_per_page' => 3,
            ));
            ?>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="item">
                <div class="iteminner">
                    <a href="<?php echo the_permalink(); ?>">
                        <div class="image">
                            <div class="layer"></div>
                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                            <img class="mainimg" src="<?php echo $featured_img_url; ?>">
                            <h3><?php echo the_title(); ?></h3>
                            <img class="arrow" src="<?php echo get_template_directory_uri(); ?>/images/ic_local_area_arrow.png">
                        </div>
                        <div class="content">
                            <p><?php echo wp_trim_words(get_the_content(), 15); ?></p>
                        </div>
                    </a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</div>


<div class="modal fade viewareavideo" id="viewareavideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ic_profile_images_remove.png">
                </button>
                
                <video id="myVideo" width="100%;" controls="true" playsinline>
                    <source src="<?php echo the_field('area_video'); ?>" type="video/mp4" />
                    <source src="<?php echo the_field('area_video'); ?>" type="video/webm" />
                </video>

            </div>
        </div>
    </div>
</div> 


<?php
get_footer(); ?>

<script>
    $('.viewareagallery .viewareagalleryitems .item label').on('click', function () {
        $('.viewareagallery .othergalery').toggle('slow');
    });
</script>

<script>
    $('.slick-carousel').slick({
      infinite: true,
      vertical:true,
      verticalSwiping:true,
      slidesToShow: 3,
      slidesToScroll: 3,
      prevArrow: $('.top-arrow'),
      nextArrow: $('.bottom-arrow')
    });
</script>

<script>
    $(".articledetails .imageslider .galleryimg .item img").on("click", function(){
        var src = $(this).attr('src');
        $(".articledetails .imageslider .mainimage img").attr('src',src);
    });
</script>

<script>
    var vid = document.getElementById("myVideo"); 
    $(".modal-body .close").click(function() {
        vid.pause();
    });
    $(document).on('click', function () {
        vid.pause();
    });
</script>
