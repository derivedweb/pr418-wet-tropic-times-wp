<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>

<style>
    button.gm-ui-hover-effect {
    visibility: hidden; 
}
</style>

<div class="storyteamtabs text-center">
    <div class="sitecontainer">
        <ul>
            <li><a href="<?php echo home_url('/'); ?>our-story/">Our Story</a></li>
            <li><a href="<?php echo home_url('/'); ?>our-team/">Meet The Team</a></li>
            <li class="active"><a href="<?php echo home_url('/'); ?>local-distributor/">Distribution List</a></li>
        </ul>
    </div>
</div>

<div class="localdistributers">
    <div class="directorymap">
         <div id="dvMap"></div>
    </div>

    <div class="directorymaplisting localdistributersopen">
        <div class="sitecontainer">
            <h1>Find Us at <br> Your Local Distributor</h1>
            <div class="distributerlisting">
                <div class="searchdirectory">
                    <div class="searchbox">
                        <input id="search-input" type="text" placeholder="Enter a location">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ic_location_search.png">
						<div id="location-error"></div>
                    </div>
                </div>
                <div class="distributeritems">
                    <div class="distributeritemsinner">
                        <div class="item">
                            <div class="image">
                                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                                <img src="<?php echo $featured_img_url; ?>">
                            </div>
                            <div class="content">
                                <h3>
                                    <?php echo the_title(); ?> 
                                    <?php $statuses = CFS()->get( 'status' );
                                    foreach ( $statuses as $key => $label ) {
                                        $status = $label;
                                    }    
                                    ?>
                                    <?php if($status == 'Open') { ?>
                                        <span><?php echo $status; ?></span>
                                    <?php } else { ?>
                                        <span class="red"><?php echo $status; ?></span>
                                    <?php } ?>

                                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/ic_distributor_phone.png"> <?php echo CFS()->get('phone'); ?></a>
                                </h3>
                                <h4><?php echo CFS()->get('address'); ?></h4>
                                <div class="tradinghour">
                                    <h6>Trading Hours</h6>
                                    <ul>
                                        <li>Thursday <b><?php echo CFS()->get('thursday'); ?></b></li>
                                        <li>Friday <b><?php echo CFS()->get('friday'); ?></b></li>
                                        <li>Saturday <b><?php echo CFS()->get('saturday'); ?></b></li>
                                        <li>Sunday <b><?php echo CFS()->get('sunday'); ?></b></li>
                                        <li>Monday <b><?php echo CFS()->get('monday'); ?></b></li>
                                        <li>Tuesday <b><?php echo CFS()->get('tuesday'); ?></b></li>
                                        <li class="closed">Wednesday <b><?php echo CFS()->get('wednesday'); ?></b></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>



<?php
get_footer(); ?>

     <script type="text/javascript">
        var markers = [
        {
            "title": '<?php echo the_title(); ?>',
            "lat": '<?php echo CFS()->get('latitude'); ?>',
            "lng": '<?php echo CFS()->get('longitude'); ?>',
            "description": '<img src="<?= $featured_img_url; ?>"><h3><?php echo the_title(); ?></h3><h4><?php echo CFS()->get('address'); ?></h4><div class="phone"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_distributor_phone.png"><label><?php echo CFS()->get('phone'); ?></label></div><?php if($status == 'Open') { ?><span>Open <b>Closes <?php echo CFS()->get('closes'); ?></b></span><?php } else { ?> <span class="red">Closed</span> <?php } ?> '
        },
        ];
        window.onload = function () {
            LoadMap();
        }
        function LoadMap() {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
     
            //Create and open InfoWindow.
            var infoWindow = new google.maps.InfoWindow();
			
			
			// NEW CODE ADDED
			var input = document.getElementById('search-input');
			var autocomplete = new google.maps.places.Autocomplete(input);

			var locationMarker = new google.maps.Marker({
				map : map
			});
			autocomplete.addListener('place_changed',function() {
				document.getElementById("location-error").style.display = 'none';
				infoWindow.close();
				locationMarker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					document.getElementById("location-error").style.display = 'inline-block';
					document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
					return;
				}

				map.fitBounds(place.geometry.viewport);
				locationMarker.setPosition(place.geometry.location);
				locationMarker.setVisible(false);

				//infowindowContent.children['place-icon'].src = place.icon;
				//infowindowContent.children['place-name'].textContent = place.name;
				//infowindowContent.children['place-address'].textContent = input.value;
				//infoWindow.open(map, locationMarker);
			});
			// NEW CODE END
     
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title,
                    icon: '<?php echo get_template_directory_uri(); ?>/images/ic_map_pin.png'
                });
     
                //Attach click event to the marker.
                (function (marker, data) {
                    
                        //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                        infoWindow.setContent("<div class='dirbox'>" + data.description + "</div>");
                        infoWindow.open(map, marker);
                    
                })(marker, data);
            }
        }
    </script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&signed_in=true&libraries=places&callback=initMap"
        async defer></script>



