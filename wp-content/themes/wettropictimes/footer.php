<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>
<?php global $wet_vars; ?>
<div class="footer">
    <div class="sitecontainer">
        <div class="footertop">
            <div class="widget aboutfooter">
            	<?php if (!empty($wet_vars['about_title'])) : ?>
                <h3><?php echo $wet_vars['about_title']; ?></h3>
                <?php endif; ?>
                <?php if (!empty($wet_vars['about_desc'])) : ?>
                <?php echo $wet_vars['about_desc']; ?>
                <?php endif; ?>
            </div>
            <div class="widget wttnews">
                <h4>WTT News</h4>
                <?php 
                    wp_nav_menu(array(
                        'theme_location' => 'header_categories',
                        'menu_class' => '',
                    ));
                ?> 
            </div>
            <div class="widget localarea">
                <h4>Local Areas</h4>
                <?php if (!empty($wet_vars['local_areas'])) : ?>
                <p><?php echo $wet_vars['local_areas']; ?></p>
                <?php endif; ?>
                <h4>Other</h4>
                <ul>
                    <li><a href="<?php echo home_url('/'); ?>local-distributor/">Distribution List</a></li>
                    <li><a href="#">Advertise with WTT</a></li>
                </ul>
            </div>
            <div class="widget downloadapp">
                <h4>Download app</h4>
                <?php if (!empty($wet_vars['apple_store_url'])) : ?>
		        <a class="applestore" href="<?php echo $wet_vars['apple_store_url']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_apple.png"></a>
		        <?php endif; ?>
		        <?php if (!empty($wet_vars['play_store_url'])) : ?>
		        <a href="<?php echo $wet_vars['play_store_url']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_play_store.png"></a>
		        <?php endif; ?>
                <ul>
                    <li><a href="<?php echo home_url('/'); ?>terms-conditions">Terms & Conditions</a></li>
                    <li><a href="<?php echo home_url('/'); ?>privacy-policy">Privacy Policy</a></li>
                    <li><a href="<?php echo home_url(); ?>/our-story">About WTT</a></li>
                    <li><a href="<?php echo home_url('/'); ?>contact-us">Contact US</a></li>
                    <li><a href="<?php echo home_url('/'); ?>our-local-areas">Local Areas</a></li>
                </ul>
            </div>
        </div>

        <div class="footerbottom">
            <?php if (!empty($wet_vars['site_logo'])) : ?>
            <div class="logo">
                <a href="<?= home_url(); ?>"><img src="<?php echo $wet_vars['site_logo']; ?>"></a>
            </div>
            <?php endif; ?>
            <div class="copyright">
            	<?php if (!empty($wet_vars['copyright'])) : ?>
                <p><?php echo $wet_vars['copyright']; ?></p>
                <?php endif; ?>
            </div>
            <div class="social">
                <ul>
                	<?php if (!empty($wet_vars['fb_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['fb_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_facebook.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($wet_vars['insta_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['insta_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_instagram.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($wet_vars['twit_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['twit_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_twitter.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                    <?php if (!empty($wet_vars['linked_url'])) : ?>
                    <li>
                    	<a target="_blank" href="<?php echo $wet_vars['linked_url']; ?>">
                    		<img src="<?php echo get_template_directory_uri(); ?>/images/ic_header_linkedin.png">
                    	</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

<!--jQuery JS-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>

<!-- <script type="text/javascript">
	
	function showLocation(position) {
		var latitude = position.coords.latitude;
		var longitude = position.coords.longitude;
		var latlongvalue = position.coords.latitude + ","
		+ position.coords.longitude;
		console.log(position);
	}
	function errorHandler(err) {
		if(err.code == 1) {
			alert("Error: Access is denied!");
		} else if( err.code == 2) {
			alert("Error: Position is unavailable!");
		}
	}
	function getLocation(){
		if(navigator.geolocation){
			// timeout at 60000 milliseconds (60 seconds)
			var options = {timeout:60000};
			navigator.geolocation.getCurrentPosition
			(showLocation, errorHandler, options);
		} else{
			alert("Sorry, browser does not support geolocation!");
		}
	}
</script> -->


<script>
	
 	$.get("https://ipinfo.io", function (response) {
		//$("#ip").html("IP: " + response.ip);
		//$("#address").html("Location: " + response.city + ", " + response.region);
 		//$('.cityname').text(response.city);
		//$("#details").html(JSON.stringify(response, null, 4));
		console.log($('.cityname').text(response.city));
		$.ajax({
			//url: "<?= home_url() ?>/weather",
			url: 'https://api.openweathermap.org/data/2.5/weather?q='+response.city+'&appid=21a598a527f436e2b6f91b4dd696695b',
			method: "GET",
			data: {city:response.city},
			success: function (data) {
				var temp = (parseFloat(data.main.temp).toFixed(2)/10).toFixed(2);
				var min_temp = (parseFloat(data.main.temp_min).toFixed(2)/10).toFixed(2);
				var max_temp = (parseFloat(data.main.temp_max).toFixed(2)/10).toFixed(2);

				$('.cityname').text(response.city);
				$('.maxtemp').text(max_temp);
				$('.mintemp').text(min_temp);

				$('.header .headertop .address #pac-input').hide();

				console.log('Temp = ' + temp);
				console.log('Temp min = ' + min_temp);
				console.log('Temp max = ' + max_temp);
			}
		});
		
 	}, "jsonp");	
	
	
	
	/*
	$.ajax({
	  url: "https://geolocation-db.com/jsonp",
	  jsonpCallback: "callback",
	  dataType: "jsonp",
	  success: function(location) {
		  
			$.ajax({
				url: 'https://api.openweathermap.org/data/2.5/weather?q='+location.city+'&appid=21a598a527f436e2b6f91b4dd696695b',
				method: "GET",
				data: {city:location.city},
				success: function (data) {
					var temp = (parseFloat(data.main.temp).toFixed(2)/10).toFixed(2);
					var min_temp = (parseFloat(data.main.temp_min).toFixed(2)/10).toFixed(2);
					var max_temp = (parseFloat(data.main.temp_max).toFixed(2)/10).toFixed(2);

					$('.cityname').text(location.city);
					$('.maxtemp').text(max_temp);
					$('.mintemp').text(min_temp);

					$('.header .headertop .address #pac-input').hide();

					console.log('Temp = ' + temp);
					console.log('Temp min = ' + min_temp);
					console.log('Temp max = ' + max_temp);
				}
			}); 
			
		*/
		  
		 /* 
		$('#country').html(location.country_name);
		$('#state').html(location.state);
		$('.cityname').html(location.city);
		$('#latitude').html(location.latitude);
		$('#longitude').html(location.longitude);
		$('#ip').html(location.IPv4);
		*/
		
	 // }
	//});
	
	
</script>	


<script>
    function openNav() {
      document.getElementById("mySidenav").style.width = "100%";
    }
    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
    }
</script> 


<script>
    $(".header .headermiddle .menus ul li.news a").hover(function(){
        $('.categorymenus').show();
    });
</script>

<script>
	$(".header .headertop .address label a").click(function(){
	  	$('.header .headertop .address #pac-input').show();
		$('.header .headertop .address .cityname').css('opacity','0');
		$('#pac-input').val($('.cityname').text());
	});
	$(document).on('click', function () {
		$('.header .headertop .address #pac-input').hide();
		$('.header .headertop .address .cityname').css('opacity','100');
	});
	
	$(".header .headertop .address").click(function (event) {
      event.stopPropagation();
	});
</script>


<script>
    function initMap() {
      var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 35.86166, lng: 104.195397},
        zoom: 6,
        scaleControl: true
      });
      var input = /** @type {!HTMLInputElement} */(
          document.getElementById('pac-input'));

      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      
      var autocomplete = new google.maps.places.Autocomplete(input, {
        types: ['(cities)']
        //componentRestrictions: {
          //country: 'CN'
        //}
      });
      autocomplete.bindTo('bounds', map);

      var infowindow = new google.maps.InfoWindow();
      var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
      });

      autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
		  
		if(place.vicinity != '' && place.vicinity != null){
			//$('.cityname').text(place.vicinity);
			$.ajax({
				//url: "<?= home_url() ?>/weather",
				url: 'https://api.openweathermap.org/data/2.5/weather?q='+place.vicinity+'&appid=21a598a527f436e2b6f91b4dd696695b',
				method: "GET",
				data: {city:place.vicinity},
				success: function (data) {
					var temp = (parseFloat(data.main.temp).toFixed(2)/10).toFixed(2);
					var min_temp = (parseFloat(data.main.temp_min).toFixed(2)/10).toFixed(2);
					var max_temp = (parseFloat(data.main.temp_max).toFixed(2)/10).toFixed(2);
					
					$('.cityname').text(place.vicinity);
					$('.maxtemp').text(max_temp);
					$('.mintemp').text(min_temp);
					
					$('.header .headertop .address #pac-input').hide();
					$('.header .headertop .address .cityname').css('opacity','100');
					
					console.log('Temp = ' + temp);
					console.log('Temp min = ' + min_temp);
					console.log('Temp max = ' + max_temp);
				}
			});
		}
		  
        if (!place.geometry) {
          window.alert("Autocomplete's returned place contains no geometry");
          return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
          map.fitBounds(place.geometry.viewport);
        } else {
          map.setCenter(place.geometry.location);
          map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setIcon(/** @type {google.maps.Icon} */({
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
          address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
        }

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
      });
    }
	
// 	$( window ).load(function() {
// 	  getLocation();
// 	});

</script>


</body>
</html>
