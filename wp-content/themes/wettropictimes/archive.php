<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

get_header();
?>
<?php global $wet_vars; ?>

<div class="categorybanner">
    <div class="layer"></div>
    <img src="<?php echo z_taxonomy_image_url(); ?>">
    <div class="heading">
        <span>News</span>
        <h1><?php echo single_cat_title(); ?></h1>
    </div>
</div>

<?php if (!empty($wet_vars['ad_img'])) : ?>
<div class="flightticketbook ticketbookother">
    <div class="sitecontainer">
        <a href="<?php echo $wet_vars['ad_url']; ?>"><img src="<?php echo $wet_vars['ad_img']; ?>"></a>
    </div>
</div>
<?php endif; ?>

<div class="regiontopstores categorystorylisting">
    <div class="sitecontainer">

        <div class="topstoryhead">
            <div class="selectbox">
                <h2>All Regions Top Stories</h2>
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>

        <div class="storieslisting archivelisting">

			<?php if ( have_posts() ) : ?>
			<?php $i = 1; ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="item <?php if($i == 1) { ?> big <?php } ?>">
                <div class="iteminner">
                    <div class="layer"></div>
                    <a href="<?php echo the_permalink(); ?>">
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'medium_large'); ?>

                        <?php if($i != 1) { ?>
                        <div class="image">
                        <?php } ?>    
                            <img src="<?php echo $featured_img_url; ?>">
                            <?php if (in_category('Videos') or in_category('Podcasts') ) { ?>  
                            <?php if($i != 1) { ?>
                            <label class="play1"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_story_video_play.png"> 3:56</label>
                            <?php } ?>
                            <?php } ?>

                        <?php if($i != 1) { ?>
                        </div>
                        <?php } ?>

                        <div class="content">
                            <?php if($i == 1) { ?>
                            <?php if (in_category('Videos') or in_category('Podcasts') ) { ?>    
                            <label class="play"><img src="<?php echo get_template_directory_uri(); ?>/images/ic_story_video_play.png"> 3:56</label>
                            <?php } ?>
                            <?php } ?>
                            <span>Posted <?php echo get_the_date(); ?></span>

                            <?php 
                            $title = wp_trim_words(get_the_title(), 100);
                            $titlesmall = substr($title, 0, 500);
                            ?>

                            <h3><?php echo $titlesmall; ?></h3>
                            <?php 
							$preview = CFS()->get('preview_text');
                            $big = wp_trim_words($preview, 50);
                            $small = substr($big, 0, 65);
                            ?>
                            <p><?php echo $small; ?></p>
                        </div>
                    </a>
                </div>
            </div>

			<?php $i++; endwhile; endif; ?>

		</div>


		<div class="loadmorestory">
            <a id="loadMore" href="javascript:void(0);">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ic_stories_load more.png"> <br>
                Load More Stories
            </a>
        </div>
		
    </div>
</div>




<?php
get_footer();
?>



<script>
	
	/*
	$(document).ready(function () {
		$('#loadMore').hide();
		size_li = $(".regiontopstores .storieslisting .item").size();
		x=7;
		if(size_li >= x){
			$('#loadMore').show();
		}
		$('.regiontopstores .storieslisting .item:lt('+x+')').show();
		$('#loadMore').click(function () {
			x= (x+5 <= size_li) ? x+5 : size_li;
			$('.regiontopstores .storieslisting .item:lt('+x+')').show();
			if(x == size_li){
				$('#loadMore').hide();
			}
		});
	});
	*/
	
	$('#loadMore').hide();
    $(document).ready(function(){
		
	  size_li = $(".regiontopstores .storieslisting .item").size();	
	  x=7;
	  if(size_li >= x){
		  $('#loadMore').show();
	  }	
	  	
      $(".regiontopstores .storieslisting .item").slice(0, 7).show();
      $("#loadMore").on("click", function(e){
        e.preventDefault();
        $(".regiontopstores .storieslisting .item:hidden").slice(0, 12).slideDown();
        if($(".regiontopstores .storieslisting .item:hidden").length == 0) {
          $("#loadMore").text("").addClass("noContent");
        }
      });
    })
	
</script>
