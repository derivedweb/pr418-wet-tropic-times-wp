let autocomplete;
let input;

function autocomplet_set_google_autocomplete() {
  input = document.querySelector(input_fields);
  autocomplete = new google.maps.places.Autocomplete(input);

  input.focus();
  autocomplete.addListener("place_changed", fillInAddress);
}

function fillInAddress() {
  const place = autocomplete.getPlace();
  jQuery(input_fields).trigger("change");
}
jQuery(window).on("load", function () {
  autocomplet_set_google_autocomplete();
});
